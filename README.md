# LymeDetector

The app is here - https://gitlab.com/Sariell/lyme_disease_app

# Structure:
**Server** folder - the Flask server, with Tensorflow adn working model integrated.

**processed_dataset** - dataset, collected from the Internet and reviewed by doctors.

**learn_model_final.ipynb** - Jupyter Notebook file with training the Neural network.

**Android app is here -** https://gitlab.com/Sariell/lyme_disease_app